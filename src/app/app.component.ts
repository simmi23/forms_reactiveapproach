import {Component, OnInit} from '@angular/core';
import {FormArray, FormControl, FormGroup, Validators} from '@angular/forms';
import {Observable, Observer} from 'rxjs';
import {resolve} from 'url';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  genders = ['male', 'female'];
  signUpForm: FormGroup;
  forbiddenUserNames = ['chris', 'Anna'];
  ngOnInit(): void {
    this.signUpForm = new FormGroup({
      userData: new FormGroup({
        // to not allow forbidden names=> this.forbiddenNames.bind(this)
        username: new FormControl(null, [Validators.required, this.forbiddenNames.bind(this)]),
        // forbiddenEmail => adding asymchronous validator
        email: new FormControl(null, [Validators.required, Validators.email], this.forbiddenEmail)
      }),
      // Validation through Validators.required or Validators Array
      /*username: new FormControl(null, Validators.required), // Grouped above
      email: new FormControl(null, [Validators.required, Validators.email]),
  */    gender: new FormControl('male'),
        hobbies: new FormArray([])
    });
    // ---------------------------------------------------------------------------------------
    // status and value changes
    this.signUpForm.valueChanges.subscribe((value) => console.log(value));
    this.signUpForm.statusChanges.subscribe((status) => console.log(status));
    // to set all controls
    this.signUpForm.setValue({
      userData: {
        username: 'Max',
        email: 't@gmail.com'
      },
      gender: 'female',
      hobbies: []
    });
    // to set specific control
    this.signUpForm.patchValue({
      userData: {
        username: 'Anna'
      }
    });
  }

  onSubmit() {
    console.log(this.signUpForm);
  }

  onAddHobby() {
    const control = new FormControl(null, Validators.required);
    (this.signUpForm.get('hobbies') as FormArray).push(control);
  }
  // create our own validator to check whether user enters a name is one of those forbidden names
  // a validator is just a function executed by angular directly when it checks
  // the validity of form control and it check that validity when u chamge the form control
  // ----------------------------------------
  // {[s: string]: boolean} return value is javascript object which is key value pair that is s and it returns boolean
  forbiddenNames(control: FormControl): {[s: string]: boolean} {
    // check value of our control matches with this forbiddenNames
    // this.forbiddenUserNames.indexOf(control.value) => returns -1 If it is not part
    if (this.forbiddenUserNames.indexOf(control.value) !== -1) {
      return {nameisForbidden: true};
    }
    // if validation is successfull return null not return {nameisForbidden: false}
    return null;
  }
  // -------------------------------
  // adding asynchronous validators
  forbiddenEmail(control: FormControl): Promise<any> | Observable<any> {
    // tslint:disable-next-line:no-shadowed-variable
    const promise = new Promise<any>((resolve, reject) => {
      setTimeout(() => {
        if (control.value === 'test@gmail.com') {
          resolve({'emailIs Forbidden': true});
        } else {
          resolve(null);
        }
      } , 1500);
    });
    return promise;
  }
}
