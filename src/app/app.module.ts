import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule  // For reactive approach
    // FormsModule, => imported for template driven approach
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
